# PyQGIS - Criar e Editar Camadas

Paulo Dias Almeida | Jorge Gustavo Rocha
Departamento de Informática, Universidade do Minho
07 de dezembro de 2020

Na ficha anterior utilizamos apenas camadas provenientes do carregamento de fontes de dados existentes. Nesta ficha vamos analisar o processo para a criação de camadas "a partir do zero", e também quais as operações de edição base que podemos efetuar sobre camadas vetoriais no QGIS.

## Criar camada

Para criar uma nova camada no QGIS vamos utilizar a classe `QgsVectorLayer` que também tínhamos utilizado para carregar uma camada de ficheiro. 

```python
# Criar camada
nova_camada = QgsVectorLayer("Point?crs=epsg:3763", "Freguesias", "memory")
```

No primeiro argumento `"Point?crs=epsg:3763"` identificamos o tipo de geometria associado à camada e o sistema de coordenadas a considerar. O segundo argumento representa o nome da camada a criar, e por fim definimos se se trata de uma camada em ficheiro ou em memória.

### Atributos da camada

Ao definirmos uma nova camada é fundamental definir também quais os atributos e respetivos tipos de dados que os elementos da camada vão conter. Para tal vamos tirar partido do `DataProvider` da camada (responsável por gerir operações de dados sobre a camada).

```python
# Obter DataProvider
dt_provider = nova_camada.dataProvider()

# Definir campos pretendidos
campos_camada = [ QgsField("Nome", QVariant.String),
QgsField("Dicofre", QVariant.String),
QgsField("Area", QVariant.Double),
QgsField("Aux", QVariant.String) ]

# Adicionar campos à camada
dt_provider.addAttributes(campos_camada)

# Notificar camada da existência de alterações de campos a serem propagadas
nova_camada.updateFields()
```

Através do `DataProvider` somos assim capazes de gerir os atributos existentes numa camada, utilizamos aqui a função `addAttributes` para definir a nossa camada. 

Caso se pretenda eliminar um atributo da camada podemos da mesma forma utilizar a função `deleteAttributes` do `DataProvider`.

```python
# Identificar campos a remover
campos_rem = [nova_camada.fields().indexOf("Aux")]

# Remover campos
dt_provider.deleteAttributes(campos_rem)

# Notificar alterações
nova_camada.updateFields()
```

Como verificamos nestes exemplos, sempre que existirem alterações aos atributos realizadas através do `DataProvider` necessitamos realizar a operação `updateFields` da camada correspondeste para essas alterações tomarem efeito.

### Gerir Elementos da camada

Tendo a nossa camada criada e os respetivos atributos definidos podemos criar e editar os elementos que vão fazer parte da mesma.

```python
# Criar elemento
feat = QgsFeature()

# Definir geometria
feat.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(0, 0)))

# Definir atributos
feat.setAttributes(["Nova", "00000", 0])

# Adicionar os novos elementos através do DataProvider
dt_provider.addFeatures([feat])

# Notificar camada da existência de alterações de elementos a serem propagadas
nova_camada.updateExtents()
```

Para a definição dos elementos de uma nova camada é muitas vezes útil utilizar outras camadas como fonte de dados e operações geométricas auxiliares.

```python
# copiar elementos de camada fonte
camada_fonte = iface.activeLayer()

for feat in camada_fonte.getFeatures():
    nova_feat = QgsFeature()
    # definir geometria como o centroid da feature base
    nova_feat.setGeometry(feat.geometry().centroid())
    # copiar atributos
    nova_feat.setFields(nova_camada.fields())
    nova_feat['Nome'] = feat["Freguesia"]
    nova_feat['Dicofre'] = feat["Dicofre"]
    nova_feat['Area'] = feat["AREA_T_Ha"]

    # Adicionar novo elemento
    dt_provider.addFeature(nova_feat)

# Notificar alterações
nova_camada.updateExtents()
```

No exemplo anterior estamos a criar um novo elemento onde a geometria resulta da operação de centroid realizada sobre uma geometria base. Podemos realizar diversas outras operações, destacando-se a simples cópia e a combinação de geometrias existentes.

```python
# copiar geometria existente
nova_feat.setGeometry(f_feat.geometry())

# unir geometrias existentes
nova_feat.setGeometry(f1_feat.geometry().combine(f2_feat.geometry()))
```

Caso se pretenda editar os atributos já presentes numa camada devemos utilizar a função `changeAttributeValues` do `DataProvider` da camada

```python
# Selecionar elementos a editar
request = QgsFeatureRequest(QgsExpression("Dicofre='00000'"))
flt_feats = nova_camada.getFeatures(request)

# Identificar indice dos atributos a editar
area_index = nova_camada.fields().indexOf("Area")

for edit_feat in flt_feats:
    # Construir dicionário com novos dados
    update = {
        edit_feat.id() : { area_index: 15986 }
    }
    # Realizar update
    dt_provider.changeAttributeValues(update)
```

Para eliminarmos elementos de uma camada podemos utilizar a função `deleteFeatures`

```python
# Selecionar elementos a eliminar
request = QgsFeatureRequest(QgsExpression("Dicofre='00000'"))
flt_feats = nova_camada.getFeatures(request)

# Agrupar ids dos elementos
ids_elm = []
for elem_feat in flt_feats:
    ids_elm.append(elem_feat.id())

# Eliminar
dt_provider.deleteFeatures(flt_feats)

# Notificar alterações
nova_camada.updateExtents()
```

## Exercícios

1. Dada a camada relativa à CAOP para Portugal continental, cria uma nova camada correspondendo ao centroid de cada freguesia e contendo os seguintes atributos ("Nome", "Concelho", "Dicofre", e "Area").

1. Dada a camada relativa à CAOP para Portugal continental, edita a camada anterior de maneira a adicionar um atributo correspondente ao perímetro (2D planar) de cada freguesia (tira partido da função `length` relativa à geometria dos elementos).

1. Dada a camada relativa à CAOP para Portugal continental, cria uma nova camada correspondendo aos concelhos representados e contendo os seguintes atributos ("Nome", "Distrito", e "Area").
