# carregar camada atual
layer = iface.activeLayer()

# propriedades da camada
valid = layer.isValid()
layerName = layer.name()
layerType = layer.type()
featureCount = layer.featureCount()

if valid:
    print("Selecionada a camada '{}' do tipo '{}' com {} elementos.".format(
        layerName, layerType, featureCount))

    # campos da camada
    fields = layer.fields()
    print("Os elementos da camada contem os seguintes atributos:")
    for fld in fields:
        print("\tCampo '{}' do tipo '{}'".format(fld.name(), fld.typeName()))

    # dicionário concelhos e respectivas áreas
    concelhosArea = {}
    for feat in layer.getFeatures():
        nome = feat['Concelho']
        if nome in concelhosArea:
            concelhosArea[nome] = concelhosArea[nome] + feat['AREA_EA_Ha']
        else:
            concelhosArea[nome] = feat['AREA_EA_Ha']

    print(concelhosArea)

    # selecionar concelho Guimarães
    guimaraes = []
    for feat in layer.getFeatures():
        if feat['Concelho'] == "GUIMARÃES":
            guimaraes.append(feat.id())

    layer.select(guimaraes)

    # selecionar concelho Guimarães [ALTERNATIVA]
    layer.removeSelection()  # remover seleção

    guimaraes = []
    request = QgsFeatureRequest(QgsExpression("Concelho='GUIMARÃES'"))
    features = layer.getFeatures(request)
    for feat in features:
        guimaraes.append(feat.id())

    layer.select(guimaraes)

    # concelhos maior área
    layer.removeSelection()  # remover seleção

    concelhosMaiorArea = sorted(
        concelhosArea, reverse=True, key=concelhosArea.get)[:5]

    ids = []
    for feat in layer.getFeatures():
        if feat['Concelho'] in concelhosMaiorArea:
            ids.append(feat.id())

    layer.select(ids)
