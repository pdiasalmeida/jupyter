from unidecode import unidecode

layer = iface.activeLayer()

resultado = {}

for f in layer.getFeatures():
    letra = unidecode(f["Concelho"][0])
    if letra in resultado:
        resultado[letra] = resultado[letra] + 1
    else:
        resultado[letra] = 1

for chave in sorted(resultado, reverse=True, key=resultado.get):
    print('Concelhos começados por {}: {}'.format(chave, resultado[chave]))
