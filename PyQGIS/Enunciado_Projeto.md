# <p align="center"> UNIVERSIDADE DO MINHO <br> Mestrado em Geografia</p>

## <p align="center"> Introdução à Programação em SIG <br> Projeto</p>

### Objetivo 

<p style="text-align:justify">
Aplicar os conhecimentos adquiridos durante o semestre no desenvolvimento de uma ferramenta baseada em PyQGIS de maneira a resolver um problema real identificado por cada grupo.
</p>
<p style="text-align:justify">
Pretende-se que cada grupo caracterize assim uma tarefa / problema vivenciados na interação com sistemas de informação geográfica (QGIS, ArcGIS), cuja complexidade ou natureza repetitiva beneficie de uma resolução através da implementação de um pequeno programa PyQGIS.
</p>

#### Temas possíveis

<p style="text-align:justify">
Dado os objetivos desta cadeira, é preferível que o objeto do projeto parta de cada grupo com base em problemas já enfrentados na utilização de sistemas de informação geográfica. No entanto, caso não ocorra um tema apropriado são apresentados de seguida, a título de exemplo, os seguintes possíveis projetos:
</p>

##### COVID

<p style="text-align:justify">
Dado as fontes de dados 
<a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:Tabela_Covid-19_Portugal_regi%C3%A3o/dia_(DGS)">disponibilizadas publicamente</a>
criar mapas caracterizados por número de infeções, por risco, etc ...
</p>

##### Dados Climáticos
<p style="text-align:justify">
O IPMA disponibiliza em dados abertos um conjunto interessante de 
<a href="https://api.ipma.pt/open-data/observation/">datasets</a>
climáticos. Outras fontes de dados também são possíveis de encontrar como o 
<a href="https://sniamb.apambiente.pt/">SNIAmb</a>
. Utilize estas fontes de dados para gerar mapas com informação sobre fenómenos afetados pelo clima, como a seca, os incêndios, etc ...
<p>

<br><br>

### Entregáveis

<p style="text-align:justify">
Para além do código-fonte relativo ao programa implementado, cada grupo deve realizar um sucinto relatório de projeto. Neste relatório deve ser caracterizado o problema que pretendem resolver, a abordagem adotada na sua resolução e as vantagens da utilização do programa desenvolvido em relação ao método original.
</p>

### Prazo

<p style="text-align:justify">
Os entregáveis associados a este projeto devem ser enviados por 
<a href="mailto:d6301@di.uminho.pt">correio eletrónico</a>
até o dia 08 de fevereiro de 2021 (inclusive).
</p>
