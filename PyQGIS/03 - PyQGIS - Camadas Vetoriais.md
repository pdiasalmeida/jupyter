# PyQGIS - Camadas

Paulo Dias Almeida | Jorge Gustavo Rocha
Departamento de Informática, Universidade do Minho
30 de novembro de 2020

Neste guia vamos explorar a utilização de camadas geográficas e dados inerentes no QGIS através de _scripts_ Python.

## Camadas Vetoriais

O QGIS permite-nos trabalhar com diversos tipo de camadas (vetoriais, imagem, _custom_), neste guia vamos nos focar em camadas vetoriais.

### Carregar camada de ficheiro

Para começar a trabalhar com uma camada no QGIS temos primeiro de carregar o ficheiro respetivo. Para realizar esta operação através do Python podemos recorrer à classe `QgsVectorLayer` ou à função `iface.addVectorLayer()`.

Estes dois métodos precisam de três argumentos:

• Caminho completo para o ficheiro
• Nome a ser utilizado para a camada
• Nome do _provider_ (responsável por ler e interpretar dados, para ficheiros normalmente `ogr`)

```python
# caminho para o ficheiro com a camada
localizacao_caop = "/home/paulo/Cont_AAD_CAOP2019/Cont_AAD_CAOP2019.shp"

# carregar camada
vlayer = QgsVectorLayer(localizacao_caop, "CAOP", "ogr")
# adicionar ao projeto QGIS
QgsProject.instance().addMapLayer(vlayer)

# ALTERNATIVA
# carregar e adicionar diretamente ao projeto no QGIS
vlayer = iface.addVectorLayer(localizacao_caop, "CAOP", "ogr")
```

### Aceder à camada atual

Como vimos anteriormente, em PyQGIS, podemos facilmente aceder à camada selecionada na aplicação QGIS através da função `iface.activeLayer()`.

```python
layer = iface.activeLayer()
print(layer)
```

### Propriedades da Camada

A partir do momento em que temos acesso a uma variável com uma camada do QGIS podemos identificar e atuar sobre as diversas propriedades da mesma.

```python
# carregar camada atual
layer = iface.activeLayer()

# propriedades da camada
valid = layer.isValid()               # Validade da camada
layerName = layer.name()              # Nome da camada
layerType = layer.type()              # Tipo de camada (Vetorial, Imagem, ...)
featureCount = layer.featureCount()   # Número de elementos

if valid:
  print("Selecionada a camada '{}' do tipo '{}' com {} elementos.".format(layerName, layerType, featureCount))
```

A variável `iface` é definida e disponibilizada pelo ambiente Python QGIS e contém diversas funções e utilidades relacionadas com a interface da aplicação. Podes descobrir mais através de `help(iface)` na consola Python do QGIS.

### Elementos, campos e atributos

Investigar os campos definidos para a camada é possível recorrendo à função `fields()`. Esta função retorna uma lista com os campos da camada.

```python
# campos da camada
fields = layer.fields()
print("Os elementos da camada contem os seguintes campos:")
for fld in fields:
  print("\tCampo '{}' do tipo '{}'".format(fld.name(), fld.typeName()))
```

Os elementos da camada são, de maneira idêntica, disponibilizados através da função `getFeatures()`. Esta função retorna um iterador com todos os elementos da camada.

Para cada elemento acedemos à lista de atributos pelo método `attributes()`, ou a cada atributo individual através do nome do mesmo.

```python
# lista de elementos da camada
elementos = list(layer.getFeatures())
primeiro = elementos[0]

print(primeiro.attributes())
print(primeiro['Dicofre'])
```

Ao trabalhar sobre os elementos de uma camada podemos criar filtros para obter sub-conjuntos de elementos para potencializar e facilitar operações posteriores. Dado um conjunto de elementos a sua seleção pode ser feita através da função `select()` executada sobre uma lista com os `ids` dos elementos a selecionar.

```python
# lista de ids
ids = []
for feat in layer.getFeatures():
  if feat['Distrito'] == "BRAGA":
    ids.append(feat.id())

# efetuar seleção
layer.select(ids)
```

O QGIS disponibiliza um mecanismo de expressões para realizar diversos tipos de filtros.

```python
# expressão de seleção
request = QgsFeatureRequest(QgsExpression("Distrito='BRAGA'"))
features = layer.getFeatures(request)
```

#### Exercícios
1. Dada a camada relativa à CAOP para Portugal continental, cria um dicionário contendo os concelhos existentes e a sua respetiva área.
1. Dada a camada relativa à CAOP para Portugal continental, seleciona as freguesias relativas ao concelho de Guimarães.
1. Dada a camada relativa à CAOP para Portugal continental, seleciona as freguesias pertencentes aos 5 concelhos com maior área.
