# QGIS

Paulo Dias Almeida | Jorge Gustavo Rocha
Departamento de Informática, Universidade do Minho
16 de novembro de 2020

O QGIS é uma aplicação gratuita e _open - source_ que oferece suporte à visualização, edição e análise de dados geoespaciais.

## Introdução

De maneira a realizar uma introdução ao QGIS, vamos começar por descarregar a CAOP a partir do seguinte [site](http://mapas.dgterritorio.pt/ATOM-download/CAOP-Cont/Cont_AAD_CAOP2019.zip).

Após terminado o download vamos descompactar o ficheiro e adicionar a camada ao QGIS. (nota: descompactar a CAOP deve resultar nos seguintes ficheiros):
```
Cont_AAD_CAOP2019.cpg
Cont_AAD_CAOP2019.dbf
Cont_AAD_CAOP2019.prj
Cont_AAD_CAOP2019.qpj
Cont_AAD_CAOP2019.shp
Cont_AAD_CAOP2019.shx
```

### Adicionar Camada ao QGIS

Existem diversas formas para adicionar camadas a um projeto QGIS, a mais simples corresponde a fazer um simples _drag & drop_ do ficheiro que pretendemos adicionar (neste caso `Cont_AAD_CAOP2019.shp`) para cima da janela da aplicação.

Em alternativa, pode-se utilizar o menu `Camada > Adicionar Camada > Adicionar camada vetorial` e indicar na opção `Fonte > Conjuntos de dados vetoriais` a localização do ficheiro que pretendemos adicionar.

![](../imagens/qgs01.png)

![](../imagens/qgs02.png)

Ao adicionar a camada, caso os ficheiros estejam todos corretos, a projeção adequada deve ser detetada automaticamente pelo QGIS. Caso seja necessário alterar a projeção, pode-se aceder ao menu `Sistema de referência de coordenadas` no canto inferior direito do mapa (evidenciado a azul na seguinte imagem).

Neste menu podemos utilizar a opção `Filtro` para procurar o sistema de coordenadas pretendido e aplica-lo ao projeto.

![](../imagens/qgs03.png)

![](../imagens/qgs04.png)

### Simbologia e Etiquetas

Caso se pretenda alterar a simbologia de uma camada no QGIS é possível utilizar a opção `Simbologia` do menu `Layer properties`. Para abrir este menu podemos fazer duplo clique sobre a camada pretendida (na lista de camadas presente na secção inferior esquerda da janela).

No menu `Simbologia` temos acesso a diversas propriedades relacionadas com a forma como a camada é apresentada que podemos editar.

![](../imagens/qgs05.png)

Através do mesmo menu `Layer properties` podemos também definir e configurar etiquetas para a nossa camada. Na seguinte imagem, a titulo de exemplo, vamos configurar etiquetas com o nome de cada freguesia visíveis a partir de uma escala escolhida.

![](../imagens/qgs06.png)

### Ferramenta Identificar Elementos e Tabela de Atributos

De maneira a explorar os elementos presentes na camada podemos utilizar diversas ferramentas. A ferramenta `Identificar Elementos`(primeiro botão evidenciado na seguinte imagem) permite clicar num elemento no mapa e abrir na secção inferior direita da janela os seus atributos.

![](../imagens/qgs07.png)

Para explorar todos os elementos da camada devemos utilizar a `Tabela de atributos` acessível através do segundo botão evidenciado na imagem anterior. No menu aberto temos acesso a várias operações realizadas sobre os campos dos elementos da camada: seleção, filtros, cálculos, e edição.

### Filtrar Elementos

A `Tabela de atributos` pode assim ser utilizada para filtrar os elementos de uma camada. A titulo de exemplo, vamos tirar partido desta funcionalidade para criar uma camada com as freguesias do concelho de Guimarães a partir da nossa camada da CAOP.

Começamos por abrir a `Tabela de atributos` e clicar no botão de filtro (1 na seguinte imagem). No menu aberto preenchemos os valores de filtro para os atributos que desejamos (2) e a forma de comparação (3). Por último vamos utilizar este filtro para selecionar os elementos desejados (4).

![](../imagens/qgs08.png)

Com os elementos pretendidos selecionados podemos então criar a nova camada. Na lista de camadas clicamos com o botão direito do rato sobre a camada com a nova seleção e selecionamos a opção `Exportar > Guardar elementos selecionados como...`. Por fim, é aberta uma janela onde devemos escolher o formato e localização do ficheiro com a camada a criar.

![](../imagens/qgs09.png)

![](../imagens/qgs10.png)

## Ferramentas de processamento

Outra forma de criar novas camadas através de operações realizadas sobre a nossa camada inicial é a partir das `Ferramentas de processamento` do QGIS.

A titulo de exemplo, vamos utilizar a ferramenta de processamento `Agregar` para obter uma camada com todos os concelhos presentes na nossa camada da CAOP.

Através do menu `Vetor > Ferramentas de geoprocessamento > Agregar...` temos acesso a uma janela onde podemos configurar a operação de agregação. Nesta janela colocamos a camada da CAOP na opção `Camada de entrada`, escolhemos o atributo `concelho` na opção `Dissolver campos`, e configuramos o ficheiro de _output_ na opção `Agregado`. Por fim, clicamos no botão `Executar` para criar a nova camada com os concelhos.

![](../imagens/qgs11.png)

![](../imagens/qgs12.png)

Existem diversos outros algoritmos de processamento para explorar (_Centroids_, _Bounding Boxes_, etc.).

### Corrigir a camada concelhos

No processamento realizado para criar a nova camada concelhos "apenas" agregamos os elementos com o atributo `concelho` em comum, não realizando nenhuma operação sobre os outros atributos. 

Desta forma, a camada concelhos tem atributos que não fazem sentido ou estão errados. Vamos utilizar a `Tabela de atributos` para corrigir a nova camada.

Começa por abrir a `Tabela de atributos` da camada concelhos e carrega no botão de edição (1 na seguinte imagem) para alterar a camada.

Para remover os campos que não fazem sentido na nova camada carrega em `Eliminar campos` (2), seleciona na janela (3) os campos a remover e carrega em `OK`.

Para adicionar um novo atributo com a área correta dos concelhos em hectares carrega no botão da calculadora de atributos (5), configura o novo campo como aparece na segunda imagem abaixo e carrega em `OK`.

Por fim, para guardar as alterações carrega no botão para salvar (4) e sai do modo de edição (1).

![](../imagens/qgs13.png)

![](../imagens/qgs14.png)
