# PyQGIS - Ferramentas de processamento QGIS

Paulo Dias Almeida | Jorge Gustavo Rocha
Departamento de Informática, Universidade do Minho
11 de janeiro de 2021

Como vimos anteriormente as ferramentas de processamento do QGIS representam uma forma poderosa de criar e alterar a nossa informação geográfica. Estas ferramentas podem, e devem, também ser utilizadas nos nossos programas PyQGIS.

## Aceder às ferramentas de processamento

As ferramentas de processamento no QGIS podem ser consultadas através do menu `Processamento > Ferramentas de processamento`. Este menu abre uma vista em árvore onde podemos analisar todas as ferramentas de processamento do QGIS.

![](../imagens/proc01.png)

No lado do Python todo o processamento é acessível através da variável `processing` que é definida e disponibilizada pelo ambiente Python QGIS. De seguida vamos verificar como utilizar a ferramenta de processamento `Polígonos para linhas` no QGIS a partir do Python.

Identificando a ferramenta que nos interessa podemos aceder à sua documentação na consola Python através do seguinte comando.

```python
processing.algorithmHelp("native:polygonstolines")
```

Este comando tem como output o seguinte texto:

```python
Polígonos para linhas (native:polygonstolines)

Converte polígonos em linhas.

Converte polígonos em linhas


----------------
Input parameters
----------------

INPUT: Camada de entrada

	Parameter type:	QgsProcessingParameterFeatureSource

	Accepted data types:
		- str: ID da camada
		- str: nome da camada
		- str: fonte da camada
		- QgsProcessingFeatureSourceDefinition
		- QgsProperty
		- QgsVectorLayer

OUTPUT: Linhas

	Parameter type:	QgsProcessingParameterFeatureSink

	Accepted data types:
		- str: ficheiro do vetor de destino, por ex. 'd:/teste.shp'
		- str: 'memory:' para armazenar o resultado temporariamente na camada de memória
		- str: usando o prefixo identificador do fornecedor vetorial e o URI de destino, por exemplo 'postgres:...' para guardar o resultado numa tabela PostGIS
		- QgsProcessingOutputLayerDefinition
		- QgsProperty

----------------
Outputs
----------------

OUTPUT:  <QgsProcessingOutputVectorLayer>
	Linhas
```

Através da documentação impressa verificamos que esta ferramenta de processamento recebe um único argumento com o nome `INPUT` e retorna uma camada vetorial.

Com esta informação podemos utilizar a ferramenta da seguinte forma:

```python
lista_camadas = QgsProject.instance().mapLayersByName('Cont_AAD_CAOP2019')

if len(lista_camadas) > 0:
    cm_poly = lista_camadas[0]

    cm_linhas = processing.run("native:polygonstolines",
                                {'INPUT': cm_poly, 'OUTPUT': 'memory:'})
```

Utilizando a variável `processing` podemos invocar assim a função `run` que nos executa a ferramenta de processamento. Esta função recebe como argumentos o identificador da ferramenta de processamento e um dicionário com as opções que analisamos na documentação acima.

Podemos aceder ao resultado do algoritmo através do atributo `OUTPUT` da variável retornada.

```python
QgsProject.instance().addMapLayer(cm_linhas['OUTPUT'])
```

## Exercícios

1. Dada a camada relativa à CAOP para Portugal continental, cria uma nova camada correspondendo às linhas que delimitam as freguesias presentes.

1. Dada a camada relativa à CAOP para Portugal continental, cria uma nova camada
correspondendo a um conjunto de pontos ao longo da fronteira de cada freguesia igualmente espaçados entre eles (nota: ver ferramenta processamento `native:pointsalonglines`).
