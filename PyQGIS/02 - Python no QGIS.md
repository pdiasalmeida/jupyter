# Python no QGIS

Paulo Dias Almeida | Jorge Gustavo Rocha
Departamento de Informática, Universidade do Minho
16 de novembro de 2020

O QGIS suporta a realização de processamentos personalizados através da definição e utilização de scripts / aplicações implementadas em Python.

## Plugins e ferramentas de processamento

A funcionalidade do QGIS pode ser estendida através de _plug-ins_ desenvolvidos em Python. As ferramentas de processamento que vimos anteriormente seguem a mesma abordagem, sendo também desenvolvidas em Python.

O instalador de _plug-in_ do QGIS permite aos utilizadores procurar, atualizar e remover plug-ins da sua aplicação QGIS.

A titulo de exemplo vamos instalar o _plug-in_ `QuickMapServices` que nos permite ter acesso a um conjunto de camadas de mapa base disponíveis na web.

Começamos por selecionar na janela do QGIS o menu `Módulos > Gerir e instalar módulos...`, de seguida, na janela do menu pesquisamos por "QuickMapServices" e carregamos em `Instalar módulo`.

![](../imagens/qgs15.png)

## A consola Python

O QGIS providencia uma consola Python integrada onde podemos realizar instruções e executar programas. Esta consola é acedida através do menu `Módulos > Consola Python`, e permite a edição (1 na seguinte imagem) e execução (2) de programas na nossa aplicação QGIS.

![](../imagens/qgs16.png)

### Interagir com camadas

Através da consola Python podemos criar programas capazes de tirar partido das ferramentas do QGIS para processar todo o tipo de informação geográfica.

Como exercício vamos identificar por que letra começam os nomes dos concelhos de Portugal Continental contabilizando o número de ocorrências de cada letra.

Para tal começamos por introduzir duas funções do Python QGIS:

* `iface.activeLayer()`

    Retorna a camada atualmente selecionada na lista de camadas. A variável `iface` é definida e disponibilizada pelo ambiente Python QGIS e contém diversas funções e utilidades relacionadas com a interface da aplicação.

* `layer.getFeatures()`

    Dada uma variável com uma camada vetorial (neste caso designada `layer`) a função `getFeatures()` retorna todos os elementos que constituem essa camada.

Com estas duas funções podemos definir o seguinte programa (`concelhos_letra.py`) e, com a camada `concelhos` selecionada na lista de camadas, executar de maneira a obter o resultado pretendido.

![](../imagens/qgs17.png)
