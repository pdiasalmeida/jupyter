Instalar Python + Jupyter no Windows através de Anaconda

## Download e Instalação

* Fazer download do toolkit [Anaconda](https://www.anaconda.com/products/individual).

![](https://md.geomaster.pt/uploads/upload_4af2d950b84e13048fc53f23f251438d.png)

![](https://md.geomaster.pt/uploads/upload_f0348d57d3556a4f2604143941f518d9.png)

* Lançar o executável descarregado e seguir o menu de instalação mantendo as opções selecionadas por defeito.

## Começar um notebook Jupyter

* Após terminada a instalação (pode ser demorada), executar o programa 'Anaconda Navigator' através do menu Start do Windows e começar um Jupyter Notebook.

![](https://md.geomaster.pt/uploads/upload_223f0e1d54b5888c56e39eae2445efa5.png)

* O Jupyter vai assim abrir numa janela do browser. Vamos começar por criar uma pasta para os nossos notebooks.

![](https://md.geomaster.pt/uploads/upload_71033c3644cd4cd080a882c3262cf9f0.png)

* De seguida podemos renomear a pasta criada.

![](https://md.geomaster.pt/uploads/upload_088b6b8b3a3cebee108c0422b6338e32.png)

* Após navegarmos para a pasta onde queremos ter os nossos notebooks podemos finalmente criar o nosso notebook.

![](https://md.geomaster.pt/uploads/upload_6ec3af81994fe7365f93c8b2067f0f08.png)
