{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variáveis e tipos em Python 🐍\n",
    "Paulo Dias Almeida | Jorge Gustavo Rocha\\\n",
    "Departamento de Informática, Universidade do Minho\\\n",
    "19 de outubro de 2020\n",
    "\n",
    "O conceito de __variável__ em programação representa um valor armazenado em memória identificado por um determinado nome (identificador).\n",
    "\n",
    "No Python as variáveis definidas são tipadas, no sentido em que as operações que podemos executar sobre elas só fazem sentido para determinados tipos de operandos. Se o tipo do operando não é o adequado, o Python interrompe a execução.\n",
    "\n",
    "Nestes exercícios vamos introduzir as variáveis, os tipos e os objectos. Estes são conceitos fundamentais para se programar em Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variáveis\n",
    "Como foi referido, as variáveis são usadas para guardar dados, para posterior utilização.\n",
    "No exemplo seguinte definem-se duas variáveis `g` e `nome` que posteriormente são usadas na função `print()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = 9.8\n",
    "nome = 'Galileu Galilei'\n",
    "print( \"A aceleração da gravidade é {} m/s^2 e já era do conhecimento do {}\".format( g, nome ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Nomes das variáveis\n",
    "\n",
    "Como em qualquer linguagem de programação, o Python tem regras muito bem definidas para os [identificadores](https://docs.python.org/3/reference/lexical_analysis.html#identifiers).\n",
    "\n",
    "1. As variáveis têm que começar com uma letra ou `_`. Não podem começar com algarismos.\n",
    "1. As variáveis só podem conter letras, algarismos e `_`. Boa notícia para os portugueses: as letras acentuadas são aceites.\n",
    "1. As letras maiúsculas são diferentes das minúsculas. Por isso, as variáveis `nome`, `Nome` e `NOME` são todas diferentes.\n",
    "1. Os nomes presentes no conjunto de _keywords_ do Python não pode ser utilizado para identificador.\n",
    "\n",
    "Exemplos de identificadores para variáveis:\n",
    "\n",
    "| Válido        |   | Inválido      | \n",
    "|:-------------:|:-:|:-------------:|\n",
    "| `data`        |   | `2vezes`      | \n",
    "| `data2`       |   | `%perc`       | \n",
    "| `ângulo`      |   | `o ângulo`    |\n",
    "| `vel_inicial` |   | `vel-inicial` | \n",
    "\n",
    "Para consultar o conjunto de palavras chave protegidas no Python podemos utilizar o comando `help`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercício\n",
    "Complete o código seguinte, de forma a demonstar que as duas variáveis são diferentes (por diferirem em maiúsculas e minúsculas)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nome = 'Manuel'\n",
    "Nome = 'Maria'\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Atribuição\n",
    "A atribuição de um valor a uma variável, como já se viu, é efetuada através do símbolo `=`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dois = 2\n",
    "tres = 3\n",
    "seis = dois * tres\n",
    "print(seis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Observação__\n",
    "Ao utilizar o símbolo `=` para atribuir valores a variáveis, temos que usar um símbolo diferente quando queremos comparar dois valores. Este símbolo é representado por `==`. Ou seja, as seguintes expressões são completamente diferentes:\n",
    "\n",
    "```python\n",
    "g = 9.8\n",
    "g == 9.8\n",
    "```\n",
    "\n",
    "A expressão `g == 9.8` dá sempre um resultado _booleano_ (`True` ou `False`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Existem também facilidades para a atribuição de variáveis. Pode-se, por exemplo, fazer várias atribuições numa só linha:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r, g, b = \"Vermelho\", \"Verde\", \"Azul\"\n",
    "print(\"Conseguimos formar qualquer cor misturando {}, {}, e {}\".format(r, g, b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pode-se, também, atribuir o mesmo valor a várias variáveis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = k = j = 7\n",
    "print(i, j, k)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercício\n",
    "Define três variáveis para representar os lados de um rectângulo de 3cm de altura por 4cm de comprimento e a sua respectiva área. Imprime a àrea do rectângulo. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tipos\n",
    "Os principais tipos predefinidos do Python são:\n",
    "\n",
    "| Categoria     | | Tipo                      | \n",
    "|:--------------|:-:|:--------------------------|\n",
    "| Numéricos     | | `int`, `float`, `complex` | \n",
    "| Texto         | | `str`                     | \n",
    "| Coleções      | |  `list`, `tuple`, `range`  |\n",
    "| Dicionário    | | `dict`                    | \n",
    "| Conjuntos     | | `set`, `frozenset`                 | \n",
    "| Booleano      | | `bool`                             | \n",
    "| Binários      | | `bytes`, `bytearray`, `memoryview` | \n",
    "| Funções       | | `builtin_function_or_method` | "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myint = 7\n",
    "myfloat = 7.0\n",
    "mystring = \"7\"\n",
    "mystring2 = '7'\n",
    "mypair = (7, 8)\n",
    "mylist = [7, 8, 9]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Observação__\n",
    "As varíaveis `mystring` e `mystring2` representam duas alternativas para definir `strings` e, contendo o mesmo valor, são consideradas como iguais pelo Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mystring == mystring2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como as variáveis em Python são tipadas, temos que ter cuidado com as operações e o tipo dos operandos:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myint = 7\n",
    "myint2 = 8\n",
    "mystring = \"7\"\n",
    "mystring2 = \"8\"\n",
    "print(myint + myint2)\n",
    "print(mystring + mystring2)\n",
    "print(myint + mystring)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para abordar estes casos existem operações que nos vão permitir efetuar conversões entre tipos de dados. Por exemplo, tentando executar o seguinte código:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import date\n",
    "ano_atual = date.today().year\n",
    "\n",
    "ano_nascimento = input()\n",
    "print(\"Você tem {} anos.\".format(ano_atual - ano_nascimento) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Porque razão o exemplo anterior corre mal? Porque a variável `ano_nascimento`, que armazena o resultado da função `input()`, é do tipo `str`. Por isso, o Python não consegue fazer a subtração: `ano_atual - ano_nascimento` entre um `int` e uma `str`.\n",
    "\n",
    "No código seguinte, assegura-se que a variável `ano_nascimento` é convertida em `int` com `int(ano_nascimento)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import date\n",
    "ano_atual = date.today().year\n",
    "\n",
    "ano_nascimento = input()\n",
    "print(\"Você tem {} anos.\".format(ano_atual - int(ano_nascimento)) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tipo de uma variável\n",
    "Para investigar o tipo de uma variável, usa-se a função pré-definida `type()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ano_nascimento = input()\n",
    "type(ano_nascimento)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "planetas = [ 'Mercúrio', 'Vénus', 'Terra', 'Marte', 'Júpiter', 'Saturno', 'Urano', 'Neptuno' ]\n",
    "type(planetas)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "planetas[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(planetas[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Uma variável em Python não tem que ter sempre o mesmo tipo. A qualquer momento pode-se atribuir um valor de outro tipo à mesma variável."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3.14159\n",
    "x = 'Salazar Slytherin'\n",
    "type(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Caso seja preciso confirmar se uma variável é de um determinado tipo, pode-se usar a função pré-definida `isinstance()`. Se primeiro quiseres verificar a documentação da função `isinstance` podes voltar a usar a função `help`, a documentação também se encontra sempre disponível [online](https://docs.python.org/3/library/functions.html#isinstance)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(isinstance)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3.14159\n",
    "isinstance(x, int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercício\n",
    "Use a função predefinida [divmod](https://docs.python.org/3/library/functions.html#divmod) para calcular o quociente e o resto da divisão de 17 por 5. \n",
    "\n",
    "Nota: A função `divmod()` retorna um `tuple` (um par)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercício\n",
    "Completa o seguinte código de maneira a verificar se o número introduzido é divisível por `2`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Introduza número a verificar:')\n",
    "num = input()\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
